import { IoMdCart } from "react-icons/io";
import clsx from 'clsx'

interface Props{
	className?: string
}

const Cart = (props: Props) => {
	return(
		<div className={clsx("cursor-pointer hover:text-black py-2", props.className)}>
			<div className="relative">
				<IoMdCart className="w-5 h-5 fill-current hover:text-black" />
				<span className="border-2 border-white absolute top-0 right-0 px-1 py-1 text-[10px] font-bold leading-none text-white transform translate-x-4 -translate-y-1/2 bg-red-600 rounded-full">99</span>
			</div>
		</div>
	)	
}

export default Cart