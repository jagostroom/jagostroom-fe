import { FaAngleDown, FaArrowDown, FaChevronDown } from 'react-icons/fa';
import Button from '../Button'
import Cart from '../Cart';
import SearchField from '../Input/Search';

const Navbar = () => {
	return (
		<div className="">
			<div className="py-1 bg-trueGray-100 text-sm text-gray-600">
				<div className="container px-7 mx-auto flex justify-end">
					<div className="flex items-center">
						<Button label="Tentang Jagos" onClick={null} variant="text"/>
						<Button label="Mitra Jagos" onClick={null} variant="text"/>
						<Button label="Customer Care" onClick={null} variant="text"/>
					</div>
				</div>
			</div>

			<div className="shadow-md">
				<div className="container mx-auto flex items-center px-7 py-3 text-gray-600 bg-white">
					<div className="text-2xl font-bold text-secondary cursor-pointer">
						<img src="/images/logo.png" alt="logo" className="h-100"/>
					</div>
					<Button variant="text" onClick={null} label="Kategori" />
					<SearchField defaultValue="" handleSubmit={null} />
					<Cart className="ml-5 mr-3" />
					<div className="group ml-5 flex items-center cursor-pointer">
						<Button variant="default" onClick={null} label="Masuk" className="mr-2" color="primary" />
						<Button variant="outlined" onClick={null} label="Daftar" color="primary" />
						{/* <div className="rounded-full bg-sky-400 h-10 w-10 text-center text-white flex justify-center items-center">
							<span>AF</span>
						</div> */}
						{/* <span className="ml-2 hover:text-black">Arno Firdaus</span>
						<FaAngleDown className="ml-2 group-hover:text-black"/> */}
					</div>
				</div>
			</div>
		</div>
	)
}

export default Navbar