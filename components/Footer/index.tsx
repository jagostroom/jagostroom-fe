import { FaFacebook, FaFacebookF, FaFacebookMessenger, FaFacebookSquare, FaInstagram, FaInstagramSquare, FaPhone, FaTwitter, FaTwitterSquare, FaWhatsapp, FaWhatsappSquare } from "react-icons/fa"

const Footer = () => {
	return(
		<div className="py-10 bg-gradient-to-r from-[#6CCB9D] to-[#0A5B73]">
			<div className="container mx-auto flex text-white items-start ">
				<div className="mx-auto">
					<img src="/images/logo.png" alt="logo"/>
				</div>
				<div className="px-7 mx-auto flex flex-col">
					<div className="text-lg font-semibold mb-5">Jago Stroom</div>
					<a href="#" className="text-sm my-1">Tentung Jagos</a>
					<a href="#" className="text-sm my-1">Mitra Jagos</a>
					<a href="#" className="text-sm my-1">Blog</a>
				</div>
				<div className="px-7 mx-auto flex flex-col">
					<div className="text-lg font-semibold mb-5">Informasi</div>
					<a href="#" className="text-sm my-1">Pengembalian Barang</a>
					<a href="#" className="text-sm my-1">Layanan dan Purna Jual</a>
					<a href="#" className="text-sm my-1">Syarat dan Ketentuan</a>
					<a href="#" className="text-sm my-1">Kebijakan Privasi</a>
					<a href="#" className="text-sm my-1">FAQ</a>
				</div>
				<div className="px-7 mx-auto">
					<div className="text-lg font-semibold mb-5">Bantuan</div>
					<div className="flex items-center">
						<FaPhone className="mr-2" />
						0812-1234-5678
					</div>
					<a href="https://wa.me/6288123456789" target="_blank" className="flex items-center">
						<FaWhatsapp className="mr-2" />
						0881-2345-6789
					</a>
					<div className="text-lg font-semibold mt-5 mb-3">Ikuti Kami</div>
					<div className="flex gap-2 text-2xl">
						<a href="#"><FaFacebookSquare /></a>
						<a href="#"><FaInstagramSquare /></a>
						<a href="#"><FaTwitterSquare /></a>
						<a href="#"><FaWhatsappSquare /></a>
					</div>
				</div>
				<div className="px-7 mx-auto">
					<img src="/images/map.png" alt="map" />
					<div className="text-sm text-center font-semibold">
						© 2019 - 2021 Jago Stroom
					</div>
				</div>
			</div>
		</div>
	)
}

export default Footer