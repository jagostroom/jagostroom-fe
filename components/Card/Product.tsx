import { FaStar } from "react-icons/fa";

interface Props{
	discount?: boolean
	priceBeforeDiscount?: number
	title: string
	price: number
	rating: number
	sold: number
}

const CardProduct = (props: Props) => {
	return(
		<div className="rounded-lg border overflow-hidden shadow-sm h-full">
			<div className="h-44">
				<img src="https://placeimg.com/380/200/nature" alt="product" className="h-full max-w-full"/>
			</div>
			<div className="p-3 flex flex-col">
				<span className="text-base font-semibold mb-0.5">Rp. {props.price}</span>
				<span className="text-base mb-0.5 truncate" title="Ini judul product panjang sekali">{props.title}</span>
				{
					props.discount && 
						<div className="flex mb-0.5 items-center">
							<div className="text-xs bg-red-500 text-white rounded py-0.5 px-1 mr-2">50%</div>
							<div className="text-xs line-through text-gray-500">Rp. {props.priceBeforeDiscount}</div>
						</div>
				}
				<div className="text-sm flex items-center">
					<FaStar size={12} className="text-yellow-400"/> 
					<span className="ml-1 text-gray-500">{props.rating}</span>
					<span className="ml-1 text-gray-500">|</span>
					<span className="ml-1 text-gray-500">Terjual {props.sold}</span>
				</div>
			</div>
		</div>
	)
}

export default CardProduct