import { MouseEventHandler } from "react"
import clsx from 'clsx'

interface Props{
	label: string,
	onClick: MouseEventHandler<HTMLDivElement>
	color?: 'primary' | 'secondary'
	variant: 'default' | 'text' | 'outlined',
	className?: string
}

const Button = (props: Props) => {
	return(
		<div className={clsx(
			props.className,
			'px-5 py-2 cursor-pointer rounded-md',
			props.variant === 'default' 
				? props.color === 'primary' 
					? 'bg-primary-default text-white hover:bg-primary-dark'
					: 'bg-secondary text-white hover:bg-yellow-500'
				: 
			props.variant === 'outlined' 
				? props.color === 'primary' 
					? 'border border-primary-default text-primary-default hover:text-primary-dark hover:bg-gray-50'
					: 'border border-secondary text-secondary hover:bg-yellow-500'
				: 'hover:text-black'
		)} onClick={props.onClick}>
			{props.label}
		</div>
	)	
}

export default Button