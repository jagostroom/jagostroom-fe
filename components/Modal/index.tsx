import clsx from "clsx"
import { ReactElement, useState } from "react"

interface Props{
	children: ReactElement,
	footer: ReactElement,
	title: string,
}

const Modal = (props: Props) => {
	const [open, setOpen] = useState(false)

	const handleOpen = () => {
			setOpen(!open)
	}

	return (
		<div 
			className={clsx(
				open ? "flex" : "hidden",
				"items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-800 bg-opacity-50 z-10 transition delay-75"
				)}
			>
			<div className="bg-white rounded-lg w-1/2">
				<div className="flex flex-col items-start p-4">
					<div className="flex items-center w-full">
						<div className="text-gray-900 font-medium text-lg">{props.title}</div>
						<svg className="ml-auto fill-current text-gray-700 w-6 h-6 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" onClick={handleOpen}>
							<path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/>
						</svg>
					</div>
					<br />
					<div>{props.children}</div>
					<br />
					<div>{props.footer}</div>
				</div>
			</div>
		</div>
	)
}

export default Modal