import React, { KeyboardEvent, useEffect, useState } from "react"
import clsx from 'clsx'
import { FaSearch, FaTimes } from "react-icons/fa"

interface Props {
  handleSubmit: Function
  defaultValue: string 
}

const SearchField = ({ defaultValue, handleSubmit }: Props) => {
  const [value, setValue] = useState("")
  const [error, setError] = useState(false)
  const [isTyping, setIsTyping] = useState(false)

  const onClickSearch = () => {
    if (value === null) {
      return
    }

    let keyword = value.trim()
    setIsTyping(false)

    if (keyword === '') {
      setError(true)
    } else {
      setError(false)
      handleSubmit({
        id: 'keyword',
        value: keyword,
      })
    }
  }

  const handleEnter = (e: KeyboardEvent<HTMLDivElement>) => {
    if (e.keyCode === 13 || e.key === 'enter') {
      onClickSearch()
    }
  }

  const handleChange = (e) => {
    const value = e.target.value
    if (value === '') {
      setIsTyping(false)
    } else {
      setIsTyping(true)
    }
    setValue(e.target.value)
  }

  useEffect(() => {
    if (defaultValue === undefined) {
      setError(true)
    } else {
      setValue(defaultValue)
      setError(false)
    }
  }, [defaultValue])

	return(
		<div className="flex-grow">
			<div
				className={clsx(
					// error && styles.errorBox,
					// error && !isTyping && styles.errorBackground,
					'border border-gray-400 rounded px-3 flex h-[46px]'
				)}
			>
				<input
					placeholder="Mau cari apa hari ini..."
					onChange={handleChange}
					value={value}
					className="focus:outline-none flex-grow bg-transparent text-[15px] w-full md:w-auto"
					maxLength={150}
					onKeyDown={handleEnter}
					data-testid="input-search"
				/>
				{value && (
					<FaTimes
						onClick={() => setValue('')}
						className="my-auto cursor-pointer mr-3"
					/>
				)}
				<div className="flex items-center pl-3 pr-1 hover:text-black cursor-pointer">
					<FaSearch />
				</div>
			</div>
			{error && (
				<div className={clsx('text-left', /* styles.errorText */)}>
					Masukkan kata kunci pencarian
				</div>
			)}
		</div>	
	)
}

export default SearchField