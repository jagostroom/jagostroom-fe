import Button from "../../components/Button"
import ItemsCarousel from 'react-items-carousel';
import { CSSProperties, useState } from "react";
import { FaChevronLeft, FaChevronRight, FaStar } from "react-icons/fa";
import CardProduct from "../../components/Card/Product";

const Product = () => {
	const products = [
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1, priceBeforeDiscount: 10000},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1, priceBeforeDiscount: 10000},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
	]
	const [activeItemIndex, setactiveItemIndex] = useState(0)

	const arrowStyles: CSSProperties = {
		position: 'absolute',
		zIndex: 2,
		top: 'calc(50% - 15px)',
		width: 32,
		height: 32,
		cursor: 'pointer',
	}

	return(
		<div className="container mx-auto my-5 border-b pb-7 px-7">
			<div className="mb-3">
				<span className="text-xl font-semibold">Produk Terbaik Kami </span>
				<span className="text-md text-primary-default cursor-pointer ml-3">Lihat semua</span>
			</div>
			<div style={{"padding":0,"maxWidth":"100%","margin":"0"}}>
				<ItemsCarousel
					infiniteLoop={false}
					gutter={8}
					activePosition={'left'}
					chevronWidth={15}
					disableSwipe={false}
					alwaysShowChevrons={false}
					numberOfCards={5}
					slidesToScroll={1}
					outsideChevron={true}
					showSlither={true}
					firstAndLastGutter={false}
					activeItemIndex={activeItemIndex}
					requestToChangeActive={(value: number) => setactiveItemIndex(value)}
					rightChevron={
						<div className="border bg-white rounded-full flex items-center justify-center text-gray-500 hover:text-black" style={{ ...arrowStyles, right: 15 }}>
							<FaChevronRight type="button" className="m-auto" />
						</div>
					}
					leftChevron={
						<div className="border bg-white rounded-full flex items-center justify-center text-gray-500 hover:text-black" style={{ ...arrowStyles, left: 15 }}>
							<FaChevronLeft type="button" className="m-auto" />
						</div>
					}
				>
					{products.map((data, i) =>
						<CardProduct key={i} {...data} />
					)}
				</ItemsCarousel>
			</div>
		</div>
	)
}

export default Product