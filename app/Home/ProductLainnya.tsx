import Button from "../../components/Button";
import CardProduct from "../../components/Card/Product";

const ProductLainnya = () => {
	const products = [
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1, priceBeforeDiscount: 10000},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1, priceBeforeDiscount: 10000},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: false, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
		{price: 7600, discount: true, title: "ini adalah title panjang sekali", sold: 12, rating: 4.1},
	]

	return(
		<div className="container mx-auto my-5 border-b pb-7 px-7">
			<div className="mb-3">
				<span className="text-xl font-semibold">Produk Terbaik Kami </span>
				<span className="text-md text-primary-default cursor-pointer ml-3">Lihat semua</span>
			</div>
			<div className="grid grid-cols-6 gap-3">
				{products.map((data, i) =>
					<CardProduct key={i} {...data} />
				)}
			</div>

			<div className="text-center flex mt-5">
				<Button variant="outlined" onClick={null} label="Selengkapnya" color="primary" className="mx-auto px-12" />
			</div>
		</div>
	)
}

export default ProductLainnya