import "react-responsive-carousel/lib/styles/carousel.min.css"
import { Carousel } from 'react-responsive-carousel'
import { CSSProperties } from "react"
import { FaChevronLeft, FaChevronRight } from "react-icons/fa"

const Banner = () => {
	const sampelBanner = [1,2,3]

	const arrowStyles: CSSProperties = {
		position: 'absolute',
		zIndex: 2,
		top: 'calc(50% - 15px)',
		width: 32,
		height: 32,
		cursor: 'pointer',
	}
	return(
		<div className="container mx-auto h-[325px] mt-7 px-7">
			<Carousel 
				showThumbs={false}
				showStatus={false}
				autoPlay={true}
				interval={5000}
				infiniteLoop={true}
				renderArrowPrev={(onClickHandler, hasPrev, label) =>
					hasPrev && (
						<div className="bg-white border rounded-full flex items-center justify-center text-gray-500 hover:text-black" style={{ ...arrowStyles, left: 15 }} onClick={onClickHandler}>
							<FaChevronLeft type="button" title={label} className="m-auto text-sm"/>
						</div>
					)
				}
				renderArrowNext={(onClickHandler, hasNext, label) =>
					hasNext && (
						<div className="bg-white border rounded-full flex items-center justify-center text-gray-500 hover:text-black" style={{ ...arrowStyles, right: 15 }} onClick={onClickHandler}>
							<FaChevronRight type="button" title={label} className="m-auto text-sm" />
						</div>
					)
				}
				>
					{ 
						sampelBanner.map((d, i) => 
							<div 
								key={i}
								className="flex justify-center h-[325px] rounded-xl"  
								style={{backgroundImage: "url('/images/banner-1.jpg')"}}	
							>
							</div>
						)
					}
			</Carousel>
		</div>
	)
}

export default Banner