import "react-responsive-carousel/lib/styles/carousel.min.css"
import { FaChevronLeft, FaChevronRight, FaRegArrowAltCircleLeft, FaRegArrowAltCircleRight } from "react-icons/fa"
import { Carousel } from "react-responsive-carousel"
import { splitArray } from "../../utlis/function"
import { CSSProperties } from "react"

const Category = () => {
	const category = [
		{label: "Fruits", icon: 1},
		{label: "Vegetables", icon: 2},
		{label: "Herbs", icon: 3},
		{label: "fruits", icon: 1},
		{label: "Panel Industri", icon: 2},
		{label: "Sakelar / Stop Kontak", icon: 3},
		{label: "MCA", icon: 1},
		{label: "Fruits", icon: 1},
		{label: "Vegetables", icon: 2},
		{label: "Herbs", icon: 3},
		{label: "fruits", icon: 1},
		{label: "Panel Industri", icon: 2},
		{label: "Sakelar / Stop Kontak", icon: 3},
		{label: "MCA", icon: 1},
	]

	const arr = splitArray(category, 7)

	const arrowStyles: CSSProperties = {
		position: 'absolute',
		zIndex: 2,
		top: 'calc(50% - 15px)',
		width: 32,
		height: 32,
		cursor: 'pointer',
	}

	return(
		<div className="container mx-auto mt-7 border-b pb-7">
			<div className="px-7">
				<span className="text-xl font-semibold">Mau kategori yang mana? </span>
				<span className="text-md text-primary-default cursor-pointer ml-3">Lihat semua</span>
			</div>
			<div className="w-full mx-auto mt-7 px-7">
				<Carousel 
					showThumbs={false}
					showStatus={false}
					showIndicators={false}
					autoPlay={true}
					interval={50000}
					infiniteLoop={true}
					renderArrowPrev={(onClickHandler, hasPrev, label) =>
						hasPrev && (
							<div className="border rounded-full flex items-center justify-center text-gray-500 hover:text-black" style={{ ...arrowStyles, left: 15 }} onClick={onClickHandler}>
								<FaChevronLeft type="button" title={label} className="m-auto"/>
							</div>
						)
					}
					renderArrowNext={(onClickHandler, hasNext, label) =>
						hasNext && (
							<div className="border rounded-full flex items-center justify-center text-gray-500 hover:text-black" style={{ ...arrowStyles, right: 15 }} onClick={onClickHandler}>
								<FaChevronRight type="button" title={label} className="m-auto" />
							</div>
						)
					}
					>
						{ 
							arr.map((d, i) => 
								<div className="grid grid-cols-7 px-10" key={i}>
									{
										d.map((e, ii) => 
											<div 
												key={ii}
												className="flex flex-col justify-start items-center cursor-pointer" 
											>
												<div className="h-24 w-24">
													<img src={"/images/category-"+e.icon+".png"} alt="category" />
												</div>
												<span className="px-5 font-medium">{e.label}</span>
											</div>
										)
									}
								</div>
							)
						}
				</Carousel>
			</div>
		</div>
	)
}

export default Category