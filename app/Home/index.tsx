import Footer from "../../components/Footer"
import Modal from "../../components/Modal"
import Navbar from "../../components/Navbar"
import Banner from "./Banner"
import Category from "./Category"
import Product from "./Product"
import ProductLainnya from "./ProductLainnya"
import Promosi from "./Promosi"

const Home = () => {
	return(
		<>
			<Navbar />

			<Banner />
			<Category />
			<Promosi />
			<Product />
			<Promosi />
			<Product />
			<ProductLainnya />
			<Footer />
			<Modal title="Login" footer={<div>tes</div>}>
				<div>
					tes
				</div>
			</Modal>
		</>
	)
}

export default Home