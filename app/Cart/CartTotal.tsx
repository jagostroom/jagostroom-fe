const CartTotal = () => {
	return(
		<div className="col-span-2 p-4">
			<div className="border flex flex-col px-5 py-3">
				<span className="font-semibold text-xl text-center w-full">Pembelian</span>
				<div className="flex mt-5">
					<div className="flex-grow">Subtotal (1 barang)</div>
					<div>Rp 1.000.000</div>
				</div>
				<div className="flex my-2">
					<div className="flex-grow">Diskon</div>
					<div className="text-red-500">Rp 100.000</div>
				</div>
				<hr />
				<div className="flex my-2">
					<div className="flex-grow font-semibold">Total</div>
					<div className="font-bold">Rp 900.000</div>
				</div>
			</div>
		</div>
	)
}

export default CartTotal