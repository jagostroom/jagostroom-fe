import Footer from "../../components/Footer"
import Modal from "../../components/Modal"
import Navbar from "../../components/Navbar"
import CartTable from "./CartTable"
import CartTotal from "./CartTotal"

const Cart = () => {
	return(
		<>
			<Navbar />
			<div className="container px-7 py-7 text-gray-600 min-h-[350px]">
				<span className="text-xl font-bold">Keranjang</span>
				<div className="grid grid-cols-6 gap-4">
					<CartTable />
					<CartTotal />
				</div>
			</div>
			<Footer />
		</>
	)
}

export default Cart