import { FaTrash, FaEdit } from 'react-icons/fa'

const CartTable = () => {
	const products = [1,2,3]

	return(
		<div className="col-span-4 mt-5 mr-7">
			<table className="w-full table-auto">
				<thead>
					<tr>
						<th className="border p-2 w-10">
							<input type="checkbox" />
						</th>
						<th className="border p-2 w-10">#</th>
						<th className="border p-2">Nama</th>
						<th className="border p-2 w-16">Qty</th>
						<th className="border p-2 w-24">Action</th>
					</tr>
				</thead>
				<tbody>
					{
						products.map((d, i) => 
							<tr key={i}>
								<td className="border px-2 py-2 text-center">
									<input type="checkbox" />
								</td>
								<td className="border px-2 py-2 text-center">{d}</td>
								<td className="border px-2 py-2">sayur-sayuran</td>
								<td className="border px-2 py-2">1</td>
								<td className="border px-2 py-2">
									<div className="flex justify-center">
										<FaEdit className="mr-2 cursor-pointer hover:text-black" />
										<FaTrash className="mr-2 cursor-pointer hover:text-black" />
									</div>
								</td>
							</tr>
						)
					}
				</tbody>
			</table>
		</div>
	)
}

export default CartTable