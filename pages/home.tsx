import Head from 'next/head'
import Home from '../app/Home'

const HomePage = () => {
	return (
		<>
			<Head>
				<title>Jago Stroom</title>
			</Head>
			<Home />
		</>
	)
}

export default HomePage