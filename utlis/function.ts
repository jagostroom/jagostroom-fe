
export function splitArray(array: Array<any>, chunk: number){
	let i:number ,
		j: number, 
		temporary: Array<any>, 
		result = []
	for (i = 0,j = array.length; i < j; i += chunk) {
			temporary = array.slice(i, i + chunk);
			result.push(temporary)
	}

	return result
}