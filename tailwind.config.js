const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}', 
    './components/**/*.{js,ts,jsx,tsx}',
    './app/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      ...colors,
      primary: { 
        default: '#1C75BC',
        light: '#B8D94F',
        dark: colors.lightBlue['700']
      },
      secondary: colors.yellow['400']
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
